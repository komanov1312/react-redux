import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { fetchCustomers } from './asyncActions/customers';
import {
  AddCustomerAction,
  RemoveCustomerAction,
} from './store/customerReducer';

function App() {
  const dispatch = useDispatch();
  const cash = useSelector((state) => state.cash.cash);
  const customers = useSelector((state) => state.customers.customers);

  const addCash = (cash) => {
    dispatch({ type: 'ADD_CASH', payload: cash });
  };
  const getCash = (cash) => {
    dispatch({ type: 'GET_CASH', payload: cash });
  };
  const addCustomer = (name) => {
    const customer = {
      name,
      id: Date.now(),
    };
    dispatch(AddCustomerAction(customer));
  };

  const removeCustomer = (customer) => {
    dispatch(RemoveCustomerAction(customer.id));
  };

  return (
    <div className='App'>
      <h1>React redux</h1>
      <div style={{ fontSize: '3rem' }}>{cash}</div>
      <div style={{ display: 'flex' }}>
        <button onClick={() => addCash(Number(prompt()))}>
          Пополнить счет
        </button>
        <button onClick={() => getCash(Number(prompt()))}>
          Снять со счета
        </button>
        <button onClick={() => addCustomer(prompt())}>Добавить клиента</button>
        <button onClick={() => dispatch(fetchCustomers())}>
          Получить клиентов из базы
        </button>
      </div>
      {customers.length ? (
        <div>
          {customers.map((customer) => (
            <div onClick={() => removeCustomer(customer)} key={customer.id}>
              {customer.name}
            </div>
          ))}
        </div>
      ) : (
        <div style={{ fontSize: '2rem', marginTop: 20 }}>
          Клиенты отсутствуют
        </div>
      )}
    </div>
  );
}

export default App;
